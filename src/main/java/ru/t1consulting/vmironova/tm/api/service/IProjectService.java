package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void clear();

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
